Source: python-parse-stages
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Peter Pentchev <roam@debian.org>,
Build-Depends:
 debhelper (>> 13.15.2~),
 dh-sequence-python3,
 dh-sequence-single-binary,
 dpkg-build-api (= 1),
 pybuild-plugin-pyproject,
 python3-all,
 python3-hatch-requirements-txt,
 python3-hatchling,
 python3-pyparsing (>> 3),
Standards-Version: 4.7.0
X-DH-Compat: 14
Homepage: https://gitlab.com/ppentchev/parse-stages
Vcs-Git: https://salsa.debian.org/python-team/packages/parse-stages.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/parse-stages
X-Style: black

Package: python3-parse-stages
Architecture: all
Depends:
 python3-pyparsing (>> 3),
Description: Parse a mini-language for selecting objects by tag or name
 This library is mostly useful for command-line parsing by other tools like
 `tox-stages` and `nox-stages`. It may be used to parse e.g. a command-line
 specification like "@check and not pylint" or "@tests or ruff" and then
 match it against a list of objects that have names and lists of tags.
 .
 This package provides the Python 3.x parse_stages module.
